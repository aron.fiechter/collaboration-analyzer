package com.codelounge.interview.collaborationanalyzer.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties("analysis")
data class CollaborationAnalyzerProperties(
    // Regex to determine which branches to exclude from the analysis
    val excludeBranchesMatching: String,
)
