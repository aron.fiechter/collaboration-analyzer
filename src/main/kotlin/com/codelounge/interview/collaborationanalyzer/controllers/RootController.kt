package com.codelounge.interview.collaborationanalyzer.controllers

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.GetMapping

@Controller
class RootController {

    @GetMapping("/")
    fun blog(model: Model): String {
        model["title"] = "Collaboration Analyzer API"
        return "root"
    }

}