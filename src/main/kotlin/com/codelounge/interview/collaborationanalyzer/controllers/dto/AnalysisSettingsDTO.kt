package com.codelounge.interview.collaborationanalyzer.controllers.dto

/**
 * Settings to be used during the "repeatAnalysis" action of the EventsAnalysisService.
 *
 * Only one setting is supported:
 * @property excludedBranchNamesRegex regex to match branches that should be excluded from the analysis.
 */
data class AnalysisSettingsDTO(val excludedBranchNamesRegex: String)
