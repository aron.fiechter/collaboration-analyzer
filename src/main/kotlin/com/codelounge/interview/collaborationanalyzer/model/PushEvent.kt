package com.codelounge.interview.collaborationanalyzer.model

import java.time.LocalDateTime

/**
 * A push event has an id.
 * It has author,
 * a branch on which it happened,
 * the number of commits included in it,
 * and a timestamp indicating when it happened.
 */
class PushEvent(
    id: Int,
    val author: Author,
    val branch: Branch,
    val numberOfCommits: Int,
    val timestamp: LocalDateTime,
) : Entity("$id") {

    /* Lazy properties */

    val project by lazy { branch.project }

    /* Conversion to DTO */

    override fun toDTO(): PushEventDTO =
        PushEventDTO(id, author.toDTO(), branch.toDTO(), numberOfCommits, timestamp.toString())

}

data class PushEventDTO(
    val id: String,
    val author: AuthorDTO,
    val branch: BranchDTO,
    val numberOfCommits: Int,
    val timestamp: String,
) : EntityDTO()
