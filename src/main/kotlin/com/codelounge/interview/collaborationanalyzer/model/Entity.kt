package com.codelounge.interview.collaborationanalyzer.model

/**
 * An abstract entity with an ID.
 * This class is to simplify implementation of Repository.
 */
sealed class Entity(val id: String) {
    abstract fun toDTO(): EntityDTO
}

/**
 * Abstract DTO of an Entity.
 */
sealed class EntityDTO