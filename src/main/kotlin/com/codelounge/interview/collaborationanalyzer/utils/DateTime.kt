package com.codelounge.interview.collaborationanalyzer.utils

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object DateTime {

    /**
     * Format for GitLab timestamps: "2021-04-26T07:40:45.757Z".
     */
    private val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")

    /**
     * Parse a datetime string in the GitLab timestamp format.
     *
     * @param dateTimeString a datetime string
     * @return LocalDateTime instance
     */
    fun parse(dateTimeString: String): LocalDateTime {
        return LocalDateTime.parse(dateTimeString, formatter)
    }
}