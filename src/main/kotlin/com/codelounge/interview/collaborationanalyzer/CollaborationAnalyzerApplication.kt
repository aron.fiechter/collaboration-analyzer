package com.codelounge.interview.collaborationanalyzer

import com.codelounge.interview.collaborationanalyzer.configuration.CollaborationAnalyzerProperties
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableConfigurationProperties(CollaborationAnalyzerProperties::class)
class CollaborationAnalyzerApplication

fun main(args: Array<String>) {
	runApplication<CollaborationAnalyzerApplication>(*args)
}
