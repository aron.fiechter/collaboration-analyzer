package com.codelounge.interview.collaborationanalyzer.services

import com.codelounge.interview.collaborationanalyzer.model.*
import com.codelounge.interview.collaborationanalyzer.repos.*
import org.springframework.stereotype.Service

/**
 * This service provides simpler access to all Repository classes.
 */
@Service
class RepositoryService(
    private val authorRepo: AuthorRepository,
    private val projectRepo: ProjectRepository,
    private val branchRepo: BranchRepository,
    private val pushEventRepo: PushEventRepository,
) {
    /**
     * Functions to get all entities of each kind.
     */

    fun authors() = authorRepo.findAll()
    fun projects() = projectRepo.findAll()
    fun branches() = branchRepo.findAll()
    fun pushEvents() = pushEventRepo.findAll()

    /**
     * Functions to get entities by id
     */
    fun authorById(id: String): Author? = authorRepo.findById(id)
    fun authorByUsername(username: String): Author? = authorRepo.findByUsername(username)
    fun projectById(id: Int): Project? = projectRepo.findById(id)
    fun branchByProjectIdAndName(projectId: Int, name: String): Branch? =
        branchRepo.findByProjectIdAndName(projectId, name)
    fun pushEventById(id: Int): PushEvent? = pushEventRepo.findById(id)

    /**
     * Clear all repositories. Useful for testing.
     */
    fun clearAll() {
        authorRepo.clear()
        projectRepo.clear()
        branchRepo.clear()
        pushEventRepo.clear()
    }

    /**
     * Store an entity of any kind.
     *
     * @param T the type of Entity
     * @param entity the entity
     * @return the stored entity
     */
    fun <T : Entity> store(entity: T): T {
        val repo: EntityRepository<T> = entity.repository()
        repo.save(entity)
        return entity
    }

    /**
     * Store many entities of one kind.
     *
     * @param T the type of the Entities
     * @param entities the entities to store
     * @return the stored entities
     */
    fun <T : Entity> storeAll(entities: List<T>): List<T> = entities.map { store(it) }

    /**
     * Get the correct repository instance for an entity.
     * There's probably a better way to do this.
     *
     * @param T the type of Entity
     * @receiver an entity
     * @return the repository for this entity
     */
    private fun <T : Entity> Entity.repository(): EntityRepository<T> {
        return when (this) {
            is Author -> authorRepo
            is Project -> projectRepo
            is Branch -> branchRepo
            is PushEvent -> pushEventRepo
        } as EntityRepository<T>
    }
}