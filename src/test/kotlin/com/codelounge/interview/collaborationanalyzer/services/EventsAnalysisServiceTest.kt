package com.codelounge.interview.collaborationanalyzer.services

import com.codelounge.interview.collaborationanalyzer.controllers.dto.AnalysisSettingsDTO
import com.codelounge.interview.collaborationanalyzer.fixture.PushEventsFixture
import com.codelounge.interview.collaborationanalyzer.model.PushEvent
import com.codelounge.interview.collaborationanalyzer.model.collaboration.Collaboration
import com.codelounge.interview.collaborationanalyzer.services.analysis.EventsAnalysisService
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest


@SpringBootTest
@DisplayName("The events ingestion service")
class EventsAnalysisServiceTest @Autowired constructor(
    val eventsAnalysisService: EventsAnalysisService,
    val repositoryService: RepositoryService,
) {

    @AfterAll
    fun clearRepos() {
        repositoryService.clearAll()
    }

    @Nested
    @DisplayName("using a simple curated dataset")
    inner class SimpleDataset {
        lateinit var foundPushEvents: List<PushEvent>

        private val foundProjects by lazy { foundPushEvents.map { it.project }.distinct() }
        private val foundAuthors by lazy { foundPushEvents.map { it.author }.distinct() }
        private val foundBranches by lazy { foundPushEvents.map { it.branch }.distinct() }
        private val foundCollaborations by lazy { foundAuthors.flatMap { it.collaborations }.toSet() }

        @BeforeAll
        fun runIngestion() {
            repositoryService.clearAll()
            PushEventsFixture.ingestSimpleDatasetUsingService(eventsAnalysisService)
            foundPushEvents = repositoryService.pushEvents()
        }

        @Test
        fun `finds all push events`() {
            assertEquals(9, foundPushEvents.size, "There should be 8 total push events")
        }

        @Test
        fun `finds all projects`() {
            assertEquals(3, foundProjects.size, "There should be 3 projects")
        }

        @Test
        fun `finds all collaborations`() {
            assertEquals(5, foundCollaborations.size, "There should be 5 collaborations")
        }

        @Test
        fun `finds all branches`() {
            assertEquals(5, foundBranches.size, "There should be 5 branches")
            val expectedBranchIds = setOf(
                "1-1-add-template-code",
                "1-2-add-repos",
                "2-1-initial-sprint",
                "3-1-initial-sprint",
                "3-2-add-model")
            val actualBranchIds = foundBranches.map { it.id }.toSet()
            assertEquals(expectedBranchIds, actualBranchIds, "The found branches should have the correct names")
        }

        @Test
        fun `finds all authors`() {
            assertEquals(3, foundAuthors.size, "There should be 3 authors")
            val expectedAuthorNames = setOf("Alice", "Bob", "Chandler")
            val actualAuthorNames = foundAuthors.map { it.username }.toSet()
            assertEquals(expectedAuthorNames, actualAuthorNames, "The found authors should have the correct names")
        }

        @Nested
        @DisplayName("repeating the analysis excluding some branches")
        inner class RepeatAnalysisTest {

            private val excludedBranchNamesRegex = "1-.*"
            lateinit var newCollaborations: List<Collaboration>

            @BeforeAll
            fun repeatAnalysis() {
                val settings = AnalysisSettingsDTO(excludedBranchNamesRegex)
                newCollaborations = eventsAnalysisService.repeatAnalysis(settings)
            }

            @Test
            fun `finds less collaborations`() {
                assertEquals(1, newCollaborations.size, "There should be 1 collaboration")
            }

            @Test
            fun `finds no collaborations on branches that should be excluded`() {
                val result = newCollaborations.none { it.branch.name.matches(excludedBranchNamesRegex.toRegex()) }
                assertTrue(result, "No collaboration should be on an excluded branch")
            }

        }
    }

    @Nested
    @DisplayName("using a real dataset")
    inner class RealDatasetTest {

        lateinit var foundPushEvents: List<PushEvent>

        val foundProjects by lazy { foundPushEvents.map { it.project }.distinct() }
        val foundAuthors by lazy { foundPushEvents.map { it.author }.distinct() }
        val foundBranches by lazy { foundPushEvents.map { it.branch }.distinct() }

        @BeforeAll
        fun runIngestion() {
            repositoryService.clearAll()
            PushEventsFixture.ingestRealDatasetUsingService(eventsAnalysisService)
            foundPushEvents = repositoryService.pushEvents()
        }


        @Nested
        @DisplayName("after ingestion")
        inner class IngestionResultTest {

            @Test
            fun `finds 41 push events`() {
                assertEquals(41, foundPushEvents.size, "There should be 41 push events")
            }

            @Test
            fun `finds only one project`() {
                assertEquals(1, foundProjects.size, "There should be only one project")
            }

            @Test
            fun `finds 4 authors`() {
                assertEquals(4, foundAuthors.size, "There should be 4 authors")
            }

            @Test
            fun `finds the correct authors`() {
                // Found with `cat p_87_01.json | jq '.[] | select(.action_name | contains("pushed to")) | .author_username' | sort | uniq`
                val expectedAuthorNames = setOf(
                    "frosty_easley",
                    "mystifying_williams",
                    "pedantic_keldysh",
                    "zen_ganguly"
                )
                val actualAuthorNames = foundAuthors.map { it.username }.toSet()
                assertEquals(expectedAuthorNames, actualAuthorNames, "The found authors should have the correct names")
            }

            @Test
            fun `finds 12 branches`() {
                assertEquals(12, foundBranches.size, "There should be 12 branches")
            }

            @Test
            fun `finds the correct branches`() {
                // Found with `cat p_87_01.json | jq '.[] | select(.action_name | contains("pushed to")) | .push_data.ref' | sort | uniq`
                val expectedBranchNames = setOf(
                    "72-add-dashboard-layout-to-session-digest",
                    "73-add-timeline-brush-to-timeline-charts",
                    "77-add-pie-chart-for-languages",
                    "81-create-a-magic-command-to-install-everything",
                    "82-add-logo-for-the-extensions-pane",
                    "83-fix-the-session-type",
                    "85-make-legend-clickable-for-diagnostic-lines",
                    "86-pipeline-artifacts-fix",
                    "87-convert-all-media-to-png",
                    "90-temporarily-disable-session-sharing",
                    "91-add-complete-session-deletion-from-view",
                    "dev"
                )
                val actualBranchNames = foundBranches.map { it.name }.toSet()
                assertEquals(expectedBranchNames, actualBranchNames, "The found branches should have the correct names")
            }

        }

        @Nested
        @DisplayName("ingests data ensuring that")
        inner class IngestionResultIntegrityTest {

            @Test
            fun `all authors only have their own push events`() {
                foundAuthors.forEach { author ->
                    assertTrue(
                        author.pushEvents.all { it.author == author },
                        "An author should only contain their own push events"
                    )
                }
            }

            @Test
            fun `all branches only have their own push events`() {
                foundBranches.forEach { branch ->
                    assertTrue(
                        branch.pushEvents.all { it.branch == branch },
                        "A branch should only contain its own push events"
                    )
                }
            }

            @Test
            fun `all projects only have their own push events`() {
                foundProjects.forEach { project ->
                    assertTrue(
                        project.pushEvents.all { it.project == project },
                        "A project should only contain its own push events"
                    )
                }
            }

            @Test
            fun `the authors of each branch have that branch in their branches`() {
                foundBranches.forEach { branch ->
                    assertTrue(
                        branch.authors.all { it.branches.contains(branch) },
                        "The authors of a branch should have that branch in their branches"
                    )
                }
            }

            @Test
            fun `the branches of each author have that author in their authors`() {
                foundAuthors.forEach { author ->
                    assertTrue(
                        author.branches.all { it.authors.contains(author) },
                        "The branches of an author should have that author in their authors"
                    )
                }
            }
        }
    }
}
