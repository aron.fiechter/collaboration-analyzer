package com.codelounge.interview.collaborationanalyzer.controllers

import com.codelounge.interview.collaborationanalyzer.model.collaboration.CollaborationDTO
import com.codelounge.interview.collaborationanalyzer.services.RepositoryService
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.hamcrest.CoreMatchers.equalTo
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DisplayName("The events controller")
class EventsAnalysisControllerTest @Autowired constructor(
    val mvc: MockMvc,
    val mapper: ObjectMapper,
    val repositoryService: RepositoryService,
) {

    lateinit var jsonString: String

    fun ingest() = mvc.post("/api/events") {
        contentType = MediaType.APPLICATION_JSON
        content = jsonString
        accept = MediaType.APPLICATION_JSON
    }

    fun repeatAnalysisExcludingBranches(excludedBranchNamesRegex: String) =
        mvc.post("/api/events/repeat_analysis") {
            contentType = MediaType.APPLICATION_JSON
            content = "{ \"excludedBranchNamesRegex\": \"$excludedBranchNamesRegex\"}"
            accept = MediaType.APPLICATION_JSON
        }

    @BeforeAll
    fun setup() {
        val jsonPath = "/data/codelounge_project_87_events/all.json"
        jsonString = javaClass.getResource(jsonPath).readText()
    }

    @AfterEach
    fun tearDown() {
        repositoryService.clearAll()
    }

    @Nested
    @DisplayName("in general")
    inner class GeneralIngestionTest {

        @Test
        fun `should ingest events and return collaborations correctly`() {
            val expectedNumberOfCollaborations = 54
            ingest().andExpect {
                status { isOk() }
                content { contentType(MediaType.APPLICATION_JSON) }
                content { jsonPath("$.length()", equalTo(expectedNumberOfCollaborations)) }
            }
        }

    }

    @Nested
    @DisplayName("after ingestion")
    inner class AfterIngestionTest {

        @BeforeEach
        fun ingestEvents() {
            ingest()
        }

        @Test
        fun `should return all found events`() {
            val expectedNumberOfEvents = 687
            mvc.get("/api/events") {
                accept = MediaType.APPLICATION_JSON
            }.andExpect {
                status { isOk() }
                content { contentType(MediaType.APPLICATION_JSON) }
                content { jsonPath("$.length()", equalTo(expectedNumberOfEvents)) }
            }
        }
    }

    @Nested
    @DisplayName("after repeating analysis excluding branches that match (dev.*|main|master)")
    inner class AfterRepeatAnalysis1Test {

        private val excludedBranchNamesRegex = "(dev.*|main|master)"
        private lateinit var collaborations: List<CollaborationDTO>

        @BeforeEach
        fun ingestEventsAndRepeatAnalysis() {
            ingest()
            val response = repeatAnalysisExcludingBranches(excludedBranchNamesRegex).andReturn()
            val itemType = object : TypeToken<List<CollaborationDTO>>() {}.type
            collaborations = Gson().fromJson(response.response.contentAsString, itemType)
        }

        @Test
        fun `should find the correct collaborations`() {
            // same as before, because the branches we excluded are the default ones
            val expectedNumberOfCollaborations = 54
            assertEquals(expectedNumberOfCollaborations, collaborations.size,
                "There should be 54 collaborations")
        }

        @Test
        fun `should find no collaborations on excluded branches`() {
            val result = collaborations.none { it.branchName.matches(excludedBranchNamesRegex.toRegex()) }
            assertTrue(result, "No collaboration should be on an excluded branch")
        }
    }

    @Nested
    @DisplayName("after repeating analysis including only  branches that match (?!dev)")
    inner class AfterRepeatAnalysis2Test {

        // This should exclude all branches that are NOT 19-interval-selection
        private val excludedBranchNamesRegex = "^(?!19-interval-selection$).*"
        private lateinit var collaborations: List<CollaborationDTO>

        @BeforeEach
        fun ingestEventsAndRepeatAnalysis() {
            ingest()
            val response = repeatAnalysisExcludingBranches(excludedBranchNamesRegex).andReturn()
            val itemType = object : TypeToken<List<CollaborationDTO>>() {}.type
            collaborations = Gson().fromJson(response.response.contentAsString, itemType)
        }

        @Test
        fun `should find the correct collaborations`() {
            // On a single branch with 4 collaborators, there will be 6 collaborations (4 choose 2)
            val expectedNumberOfCollaborations = 6
            assertEquals(expectedNumberOfCollaborations, collaborations.size,
                "There should be 6 collaborations")
        }

        @Test
        fun `should find collaborations between the expected authors`() {
            // found with
            // cat all.json | jq '.[] | select(.action_name | contains("pushed to", "pushed new")) | select(.push_data.ref | contains("19-interval-selection")) | .author_username' | sort | uniq
            val expectedAuthorNames = setOf(
                "frosty_easley",
                "loving_booth",
                "mystifying_williams",
                "pedantic_keldysh",
            )
            val actualAuthorNames = collaborations
                .flatMap { setOf(it.author1.username, it.author2.username) }
                .toSet()
            assertEquals(expectedAuthorNames, actualAuthorNames,
                "The authors contributing on dev should be $expectedAuthorNames")
        }

        @Test
        fun `should find no collaborations on excluded branches`() {
            val result = collaborations.none { it.branchName.matches(excludedBranchNamesRegex.toRegex()) }
            assertTrue(result, "No collaboration should be on an excluded branch")
        }

    }
}
