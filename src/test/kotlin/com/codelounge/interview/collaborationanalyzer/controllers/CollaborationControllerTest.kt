package com.codelounge.interview.collaborationanalyzer.controllers

import com.codelounge.interview.collaborationanalyzer.model.Author
import com.codelounge.interview.collaborationanalyzer.model.AuthorDTO
import com.codelounge.interview.collaborationanalyzer.model.BranchDTO
import com.codelounge.interview.collaborationanalyzer.model.Project
import com.codelounge.interview.collaborationanalyzer.model.collaboration.CollaborationAuthorsDTO
import com.codelounge.interview.collaborationanalyzer.model.collaboration.CollaborationDTO
import com.codelounge.interview.collaborationanalyzer.services.CollaborationServiceTest
import com.codelounge.interview.collaborationanalyzer.services.RepositoryService
import com.fasterxml.jackson.databind.ObjectMapper
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.*
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.Arguments.arguments
import org.junit.jupiter.params.provider.MethodSource
import org.junit.jupiter.params.provider.ValueSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DisplayName("The collaboration controller")
class CollaborationControllerTest @Autowired constructor(
    val mvc: MockMvc,
    val mapper: ObjectMapper,
    val repositoryService: RepositoryService,
) {

    val alice = AuthorDTO("1", "Alice")
    val bob = AuthorDTO("2", "Bob")
    val chandler = AuthorDTO("3", "Chandler")

    @BeforeEach
    fun setup() {
        val jsonPath = "/data/simple/small_project_events.json"
        val jsonString = javaClass.getResource(jsonPath).readText()
        mvc.post("/api/events") {
            contentType = MediaType.APPLICATION_JSON
            content = jsonString
            accept = MediaType.APPLICATION_JSON
        }
    }

    @AfterEach
    fun tearDown() {
        repositoryService.clearAll()
    }

    @Nested
    @DisplayName("return the correct pairs of collaborators")
    inner class CollaboratorsOfProjectTest {

        /**
         * Source for parametrized test, all three projects are considered.
         */
        fun pairsOfCollaboratorsArgumentsProvider(): List<Arguments> = listOf(
            arguments(1, setOf(
                CollaborationAuthorsDTO(setOf(alice, chandler)),
                CollaborationAuthorsDTO(setOf(bob, chandler)),
                CollaborationAuthorsDTO(setOf(alice, bob)),
            )),
            arguments(2, setOf(
                CollaborationAuthorsDTO(setOf(bob, chandler)),
            )),
            arguments(3, emptySet<CollaborationAuthorsDTO>()))

        @ParameterizedTest(name = "for project {0}")
        @MethodSource("pairsOfCollaboratorsArgumentsProvider")
        fun `for a project`(
            projectId: Int,
            expectedCollaboratorPairs: Set<CollaborationAuthorsDTO>,
        ) {
            mvc.get("/api/collaborations/project/$projectId/pairs")
                .andExpect {
                    status { isOk() }
                    content { contentType(MediaType.APPLICATION_JSON) }
                    content { json(mapper.writeValueAsString(expectedCollaboratorPairs)) } }
        }

        @Test
        fun `or not found for a non-existing project id`() {
            mvc.get("/api/collaborations/project/666/pairs")
                .andExpect { status { isNotFound() } }
        }
    }

    @Nested
    @DisplayName("returns the correct collaborators")
    inner class CollaboratorsOfAuthorInProjectTest {

        /**
         * Source for parametrized test, all combinations of author and project are considered.
         */
        fun collaboratorsOfAuthorInProjectArgumentsProvider(): List<Arguments> = listOf(
            // Project 1
            arguments(1, "Alice", setOf(bob, chandler)),
            arguments(1, "Bob", setOf(alice, chandler)),
            arguments(1, "Chandler", setOf(alice, bob)),
            // Project 2
            arguments(2, "Alice", emptySet<AuthorDTO>()),
            arguments(2, "Bob", setOf(chandler)),
            arguments(2, "Chandler", setOf(bob)),
            // Project 3
            arguments(3, "Alice", emptySet<AuthorDTO>()),
            arguments(3, "Bob", emptySet<AuthorDTO>()),
            arguments(3, "Chandler", emptySet<AuthorDTO>()),
        )

        @ParameterizedTest(name = "for {1} in project {0}")
        @MethodSource("collaboratorsOfAuthorInProjectArgumentsProvider")
        fun `for an author in a project`(
            projectId: Int,
            authorUsername: String,
            expectedCollaborators: Set<AuthorDTO>,
        ) {
            mvc.get("/api/collaborations/project/$projectId/collaborators/$authorUsername")
                .andExpect {
                    status { isOk() }
                    content { contentType(MediaType.APPLICATION_JSON) }
                    content { json(mapper.writeValueAsString(expectedCollaborators)) } }
        }

        @Test
        fun `or not found for a non-existing project id`() {
            mvc.get("/api/collaborations/project/666/collaborators/Alice")
                .andExpect { status { isNotFound() } }
        }

        @Test
        fun `or not found for a non-existing author username`() {
            mvc.get("/api/collaborations/project/1/collaborators/EmilyKaldwin")
                .andExpect { status { isNotFound() } }
        }
    }

    @Nested
    @DisplayName("returns the correct collaborations")
    inner class CollaborationsOfAuthorsInProjectTest {

        /**
         * Source for parametrized test, all combinations of pairs of authors in a project are considered.
         */
        fun collaborationsOfAuthorsInProjectArgumentsProvider(): List<Arguments> = listOf(

            // Project 1 - everyone collaborated with everyone, Alice and Chandler twice
            arguments(1, "Alice", "Bob", listOf(
                CollaborationDTO(alice, bob, "1-add-template-code", "1", 7,
                    "2021-06-01T00:00", "2021-06-02T00:00"))),
            arguments(1, "Alice", "Chandler", listOf(
                CollaborationDTO(alice, chandler, "1-add-template-code", "1", 6,
                    "2021-06-01T00:00", "2021-06-03T00:00"),
                CollaborationDTO(alice, chandler, "2-add-repos", "1", 30,
                    "2021-06-04T00:00", "2021-06-05T00:00"))),
            arguments(1, "Bob", "Chandler", listOf(
                CollaborationDTO(bob, chandler, "1-add-template-code", "1", 5,
                    "2021-06-02T00:00", "2021-06-03T00:00"))),

            // Project 2 - only Bob and Chandler collaborated
            arguments(2, "Alice", "Bob", emptyList<CollaborationDTO>()),
            arguments(2, "Alice", "Chandler", emptyList<CollaborationDTO>()),
            arguments(2, "Bob", "Chandler", listOf(
                CollaborationDTO(bob, chandler, "1-initial-sprint", "2", 27,
                    "2021-06-06T00:00", "2021-06-07T00:00"))),

            // Project 3 - no collaborations
            arguments(3, "Alice", "Bob", emptyList<CollaborationDTO>()),
            arguments(3, "Alice", "Chandler", emptyList<CollaborationDTO>()),
            arguments(3, "Bob", "Chandler", emptyList<CollaborationDTO>()),
        )

        @ParameterizedTest(name = "for {1} and {2} in project {0}")
        @MethodSource("collaborationsOfAuthorsInProjectArgumentsProvider")
        fun `for two authors in a project`(
            projectId: Int,
            author1Username: String,
            author2Username: String,
            expectedCollaborations: List<CollaborationDTO>
        ) {
            mvc.get("/api/collaborations/project/$projectId/author1/$author1Username/author2/$author2Username")
                .andExpect {
                    status { isOk() }
                    content { contentType(MediaType.APPLICATION_JSON) }
                    content { json(mapper.writeValueAsString(expectedCollaborations)) } }
        }

        @ParameterizedTest
        @ValueSource(strings = [
            "project/666/author1/Alice/author2/Bob",
            "project/1/author1/EmilyKaldwin/author2/Bob",
            "project/1/author1/Alice/author2/EmilyKaldwin",
        ])
        fun `or not found for a non-existing project id or author usernames`(url: String) {
            mvc.get("/api/collaborations$url")
                .andExpect { status { isNotFound() } }
        }

    }
}
