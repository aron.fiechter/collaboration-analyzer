package com.codelounge.interview.collaborationanalyzer.fixture

import com.codelounge.interview.collaborationanalyzer.controllers.dto.InputEventDTO
import com.codelounge.interview.collaborationanalyzer.services.analysis.EventsAnalysisService
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

object PushEventsFixture {

    /**
     * Ingest real dataset
     */
    fun ingestRealDatasetUsingService(eventsAnalysisService: EventsAnalysisService) {
        val jsonPath = "/data/codelounge_project_87_events/p_87_01.json"
        ingest(jsonPath, eventsAnalysisService)
    }

    /**
     * Ingest simple curated dataset
     */
    fun ingestSimpleDatasetUsingService(eventsAnalysisService: EventsAnalysisService) {
        val jsonPath = "/data/simple/small_project_events.json"
        ingest(jsonPath, eventsAnalysisService)
    }

    private fun ingest(datasetFilePath: String, service: EventsAnalysisService) {
        // Read input data
        val jsonString = javaClass.getResource(datasetFilePath).readText()
        val itemType = object : TypeToken<List<InputEventDTO>>() {}.type
        val inputEventDTOs: List<InputEventDTO> = Gson().fromJson(jsonString, itemType)

        // Ingest data, simulate data duplication, shuffle (in a way that produces a consistent shuffle)
        val inputDataWithDuplicates = (inputEventDTOs + inputEventDTOs + inputEventDTOs)
            .shuffled(java.util.Random(42L))
        service.ingestAndAnalyseEvents(inputDataWithDuplicates)
    }
}
