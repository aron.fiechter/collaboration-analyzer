package com.codelounge.interview.collaborationanalyzer.model

import com.codelounge.interview.collaborationanalyzer.fixture.PushEventsFixture
import com.codelounge.interview.collaborationanalyzer.services.RepositoryService
import com.codelounge.interview.collaborationanalyzer.services.analysis.EventsAnalysisService
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@DisplayName("A branch")
class BranchTest @Autowired constructor(
    private val eventsAnalysisService: EventsAnalysisService,
    private val repositoryService: RepositoryService,
) {

    lateinit var branch: Branch
    lateinit var project1: Project
    lateinit var alice: Author
    lateinit var bob: Author
    lateinit var chandler: Author

    @BeforeAll
    fun setUp() {
        PushEventsFixture.ingestSimpleDatasetUsingService(eventsAnalysisService)
        branch = repositoryService.branchByProjectIdAndName(1, "1-add-template-code")!!
        project1 = repositoryService.projectById(1)!!
        alice = repositoryService.authorByUsername("Alice")!!
        bob = repositoryService.authorByUsername("Bob")!!
        chandler = repositoryService.authorByUsername("Chandler")!!
    }

    @AfterAll
    fun tearDown() {
        repositoryService.clearAll()
    }

    @Nested
    @DisplayName("should have properties with the expected values")
    inner class BranchPropertyTest {

        @Test
        fun `name = 1-add-template-code`() {
            val expectedName = "1-add-template-code"
            assertEquals(expectedName, branch.name, "The name should be correct")
        }

        @Test
        fun `project = project1`() {
            val expectedProject = project1
            assertEquals(expectedProject, branch.project, "The project should be correct")
        }

        @Test
        fun `push events size = 3`() {
            val expectedNumberOfPushEvents = 3
            assertEquals(expectedNumberOfPushEvents, branch.pushEvents.size,
                "The number of push events should be 3")
        }

        @Test
        fun `authors = { alice, bob, chandler }`() {
            val expectedAuthors = setOf(alice, bob, chandler)
            val actualAuthors = branch.authors
            assertEquals(expectedAuthors, actualAuthors,
                "The authors should be correct")
        }
    }

    @Nested
    @DisplayName("should have methods that work correctly")
    inner class BranchMethodTest {

        @Test
        fun `extraction of collaborator pairs`() {
            val expectedCollaboratorPairs = setOf(setOf(alice, bob), setOf(alice, chandler), setOf(bob, chandler))
            val actualCollaboratorPairs = branch.collaboratorPairs()
            assertEquals(expectedCollaboratorPairs, actualCollaboratorPairs,
                "The found collaborator pairs should be correct")
        }

        @Test
        fun `conversion to DTO`() {
            val dto = branch.toDTO()
            assertEquals(branch.id, dto.id, "The id should be the same")
            assertEquals(branch.name, dto.name, "The name should be the same")
            assertEquals(branch.project.id, dto.project.id, "The project id should be the same")
            assertEquals(branch.pushEvents.size, dto.numberOfPushEvents, "The number of push events should be the same")
        }
    }
}