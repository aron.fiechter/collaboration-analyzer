package com.codelounge.interview.collaborationanalyzer.model.collaboration

import com.codelounge.interview.collaborationanalyzer.fixture.PushEventsFixture
import com.codelounge.interview.collaborationanalyzer.model.Author
import com.codelounge.interview.collaborationanalyzer.model.Branch
import com.codelounge.interview.collaborationanalyzer.model.Project
import com.codelounge.interview.collaborationanalyzer.services.RepositoryService
import com.codelounge.interview.collaborationanalyzer.services.analysis.EventsAnalysisService
import com.codelounge.interview.collaborationanalyzer.utils.DateTime
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@DisplayName("A Collaboration")
class CollaborationTest @Autowired constructor(
    private val eventsAnalysisService: EventsAnalysisService,
    private val repositoryService: RepositoryService,
) {

    lateinit var project1: Project
    lateinit var alice: Author
    lateinit var bob: Author
    lateinit var branch: Branch
    lateinit var collaboration: Collaboration

    @BeforeAll
    fun setUp() {
        PushEventsFixture.ingestSimpleDatasetUsingService(eventsAnalysisService)
        alice = repositoryService.authorByUsername("Alice")!!
        bob = repositoryService.authorByUsername("Bob")!!
        project1 = repositoryService.projectById(1)!!
        branch = repositoryService.branchByProjectIdAndName(1, "1-add-template-code")!!
        collaboration = project1.collaborationsBetweenAuthors(alice, bob).first()
    }

    @Nested
    @DisplayName("should have properties with the expected values")
    inner class CollaborationPropertyTest {

        @Test
        fun `author 1, author 2 = alice and bob (in any order)`() {
            val expectedAuthors = setOf(alice, bob)
            val actualAuthors = setOf(collaboration.author1, collaboration.author2)
            assertEquals(expectedAuthors, actualAuthors, "The authors should be correct")
        }

        @Test
        fun branch() {
            assertEquals(branch, collaboration.branch, "The branch should be correct")
        }

        @Test
        fun project() {
            assertEquals(project1, collaboration.project, "The project should be correct")
        }

        @Test
        fun `number of commits`() {
            assertEquals(7, collaboration.numberOfCommits, "The number of commits should be correct")
        }

        @Test
        fun `start timestamp`() {
            assertEquals(DateTime.parse("2021-06-01T00:00:00.000Z"), collaboration.startTimestamp,
                "The start timestamp should be correct")
        }

        @Test
        fun `end timestamp`() {
            assertEquals(DateTime.parse("2021-06-02T00:00:00.000Z"), collaboration.endTimestamp,
                "The end timestamp should be correct")
        }

    }

    @Nested
    @DisplayName("should have methods that work correctly")
    inner class CollaborationMethodTest {

        @Test
        fun `collaborator of`() {
            assertEquals(bob, collaboration.collaboratorOf(alice), "The collaborator of alice should be bob")
            assertEquals(alice, collaboration.collaboratorOf(bob), "The collaborator of bob should be alice")
        }

        @Test
        fun `author set`() {
            val expectedAuthorSet = setOf(alice, bob)
            assertEquals(expectedAuthorSet, collaboration.authorsSet(), "The author set should be alice and bob")
        }

        @Test
        fun `conversion to full DTO`() {
            val dto = collaboration.toFullDTO()
            assertEquals(collaboration.author1.toDTO(), dto.author1, "The author 1 should be the same")
            assertEquals(collaboration.author2.toDTO(), dto.author2, "The author 2 should be the same")
            assertEquals("1-add-template-code", dto.branchName, "The branch name should be correct")
            assertEquals("2021-06-01T00:00", dto.startTimestamp, "The start timestamp should be correct")
            assertEquals("2021-06-02T00:00", dto.endTimestamp, "The end timestamp should be correct")
        }

        @Test
        fun `conversion to authors DTO`() {
            val dto = collaboration.toAuthorsDTO()
            val expectedAuthors = setOf(alice.toDTO(), bob.toDTO())
            assertEquals(expectedAuthors, dto.authors, "The authors should be the same")
        }

    }
}