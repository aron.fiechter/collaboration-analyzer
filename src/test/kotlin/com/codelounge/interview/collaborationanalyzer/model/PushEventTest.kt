package com.codelounge.interview.collaborationanalyzer.model

import com.codelounge.interview.collaborationanalyzer.fixture.PushEventsFixture
import com.codelounge.interview.collaborationanalyzer.services.RepositoryService
import com.codelounge.interview.collaborationanalyzer.services.analysis.EventsAnalysisService
import com.codelounge.interview.collaborationanalyzer.utils.DateTime
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@DisplayName("A push event")
class PushEventTest @Autowired constructor(
    private val eventsAnalysisService: EventsAnalysisService,
    private val repositoryService: RepositoryService,
) {

    lateinit var pushEvent: PushEvent
    lateinit var alice: Author
    lateinit var branch1: Branch
    lateinit var project1: Project

    @BeforeAll
    fun setUp() {
        PushEventsFixture.ingestSimpleDatasetUsingService(eventsAnalysisService)
        pushEvent = repositoryService.pushEventById(1)!!
        alice = repositoryService.authorByUsername("Alice")!!
        branch1 = repositoryService.branchByProjectIdAndName(1, "1-add-template-code")!!
        project1 = repositoryService.projectById(1)!!
    }

    @AfterAll
    fun tearDown() {
        repositoryService.clearAll()
    }

    @Nested
    @DisplayName("should have properties with the expected values")
    inner class PushEventPropertyTest {

        @Test
        fun `id = 1`() {
            val expectedId = "1"
            assertEquals(expectedId, pushEvent.id, "The id should be correct")
        }

        @Test
        fun `author = Alice`() {
            val expectedAuthor = alice
            assertEquals(expectedAuthor, pushEvent.author, "The author should be alice")
        }

        @Test
        fun `branch = 1-add-template-code`() {
            val expectedBranch = branch1
            assertEquals(expectedBranch, pushEvent.branch, "The branch should be correct")
        }

        @Test
        fun `number of commits = 4`() {
            val expectedNumberOfCommits = 4
            assertEquals(expectedNumberOfCommits, pushEvent.numberOfCommits,
                "The number of commits should be correct")
        }

        @Test
        fun `timestamp should be correct`() {
            val expectedTimestamp = DateTime.parse("2021-06-01T00:00:00.000Z")
            assertEquals(expectedTimestamp, pushEvent.timestamp, "The timestamp should be correct")
        }

        @Test
        fun `project = project1`() {
            val expectedProject = project1
            assertEquals(expectedProject, pushEvent.project, "The project should be correct")
        }
    }

    @Nested
    @DisplayName("should have methods that work correctly")
    inner class PushEventMethodTest {

        @Test
        fun `should be convertible to a DTO`() {
            val dto = pushEvent.toDTO()
            assertEquals(pushEvent.id, dto.id, "The id should be the same")
            assertEquals(pushEvent.author.toDTO(), dto.author, "The author should be the same")
            assertEquals(pushEvent.branch.toDTO(), dto.branch, "The branch should be the same")
            assertEquals(pushEvent.numberOfCommits, dto.numberOfCommits, "The number of commits should be the same")
            assertEquals(pushEvent.timestamp.toString(), dto.timestamp, "The timestamp should be the same")
        }
    }
}